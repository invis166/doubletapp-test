FROM python:3.10.2-slim-buster

ENV PYTHONONBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1


RUN apt-get update && apt-get install make

WORKDIR /code


COPY Pipfile Pipfile.lock /code/

RUN python3.10 -m pip install --user pipenv \
 && python3.10 -m pipenv install --system --deploy --ignore-pipfile

	
COPY . /code/

migrate:
	docker-compose run web python3 src/manage.py migrate $(if $m, api $m,)

createsuperuser:
	docker-compose run web python3 src/manage.py createsuperuser

collectstatic:
	python3 src/manage.py collectstatic --no-input

command:
	docker-compose run web python3 src/manage.py ${c}

shell:
	pipenv run python3 src/manage.py shell

debug:
	pipenv run python3 src/manage.py debug

ci_lint:
	docker-compose run web isort .
	docker-compose run web flake8 --config setup.cfg
	docker-compose run web black --config pyproject.toml .

ci_check_lint:
	docker-compose run web isort --check --diff .
	docker-compose run web flake8 --config setup.cfg
	docker-compose run web black --check --config pyproject.toml .

startbot:
	python3 src/manage.py start_bot

startbot_dev:
	python3 src/manage.py start_bot_dev

runtests:
	docker-compose run web bash -c "cd src/tests && pytest"

dev:
	python3 src/manage.py runserver 0.0.0.0:8000

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

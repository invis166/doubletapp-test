from django.contrib import admin

from app.internal.models.card import Card


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ["card_number", "get_account_number"]

    def get_account_number(self, obj: Card):
        return obj.bank_account.account_number

    get_account_number.short_description = "Account number"

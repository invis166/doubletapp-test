from django.contrib import admin

from app.internal.models.bot_user import BotUser


@admin.register(BotUser)
class UserAdmin(admin.ModelAdmin):
    list_display = ("user_name", "telegram_id")

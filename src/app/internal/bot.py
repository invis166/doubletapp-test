from telegram.ext import CommandHandler, Dispatcher, Updater

import app.internal.transport.bot.handlers.bank.balance as balance_handlers
import app.internal.transport.bot.handlers.bank.transactions as transactions_handlers
import app.internal.transport.bot.handlers.favourite as favourite_handlers
import app.internal.transport.bot.handlers.user as user_handlers


class TelegramBot:
    def __init__(self, token: str):
        self._token = token
        self._updater = Updater(token=self._token, use_context=True)
        self._configure_dispatcher()

    def run_polling(self):
        self._updater.start_polling()
        self._updater.idle()

    def run_webhook(self, domain: str, port: int):
        self._updater.start_webhook(
            listen="0.0.0.0", port=port, url_path=self._token, webhook_url=f"https://{domain}/{self._token}"
        )
        self._updater.idle()

    def _configure_dispatcher(self):
        """Set up command handlers"""
        dispatcher: Dispatcher = self._updater.dispatcher
        dispatcher.add_handler(CommandHandler("start", user_handlers.start))
        dispatcher.add_handler(CommandHandler("me", user_handlers.me))
        dispatcher.add_handler(CommandHandler("set_phone", user_handlers.set_phone))
        dispatcher.add_handler(CommandHandler("account_balance", balance_handlers.account_balance))
        dispatcher.add_handler(CommandHandler("card_balance", balance_handlers.card_balance))
        dispatcher.add_handler(CommandHandler("transfer_by_id", transactions_handlers.transfer_money_telegram_id))
        dispatcher.add_handler(CommandHandler("transfer_by_card", transactions_handlers.transfer_money_card_number))
        dispatcher.add_handler(
            CommandHandler("transfer_by_account", transactions_handlers.transfer_money_account_number)
        )
        dispatcher.add_handler(CommandHandler("add_favourite", favourite_handlers.add_to_favourite))
        dispatcher.add_handler(CommandHandler("delete_favourite", favourite_handlers.delete_from_favourite))
        dispatcher.add_handler(CommandHandler("list_favourite", favourite_handlers.get_favourite_list))
        dispatcher.add_handler(CommandHandler("account_statement", transactions_handlers.account_statement))
        dispatcher.add_handler(CommandHandler("card_account_statement", transactions_handlers.card_account_statement))
        dispatcher.add_handler(CommandHandler("interacted", transactions_handlers.interacted_users))

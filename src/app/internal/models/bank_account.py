from django.db import models

from app.internal.models.bot_user import BotUser


class BankAccount(models.Model):
    owner = models.ForeignKey(
        BotUser,
        on_delete=models.CASCADE,
    )
    account_number = models.CharField(
        unique=True,
        max_length=100,
    )
    balance = models.DecimalField(
        default=0,
        max_digits=19,
        decimal_places=4,
    )
    transactions = models.ManyToManyField(
        "BankAccount",
        through="Transaction",
        blank=True,
    )

    def __str__(self):
        return f"Bank Account: {self.account_number}"

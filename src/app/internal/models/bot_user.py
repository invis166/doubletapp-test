from django.db import models


class BotUser(models.Model):
    telegram_id = models.IntegerField(
        primary_key=True,
    )
    user_name = models.CharField(
        unique=True,
        null=True,
        max_length=255,
    )
    first_name = models.CharField(
        max_length=255,
    )
    phone_number = models.CharField(
        max_length=255,
        default="",
    )
    favourites = models.ManyToManyField(
        "BotUser",
        blank=True,
    )

    class Meta:
        verbose_name = "TelegramUser"

    def __str__(self):
        return f"{self.user_name}"

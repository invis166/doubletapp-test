from django.db import models

from app.internal.models.bank_account import BankAccount


class Card(models.Model):
    bank_account = models.ForeignKey(
        BankAccount,
        on_delete=models.CASCADE,
    )
    card_number = models.CharField(
        unique=True,
        null=False,
        max_length=100,
    )

    def __str__(self):
        return f"Card: {self.card_number}"

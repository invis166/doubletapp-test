from django.db import models

from app.internal.models.bot_user import BotUser


class Favourites(models.Model):
    owner = models.ForeignKey(
        BotUser,
        on_delete=models.CASCADE,
        related_name="owner",
    )
    favourite = models.ForeignKey(
        BotUser,
        on_delete=models.CASCADE,
        related_name="favourite",
        null=False,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["favourite", "owner"],
                name="unique_favourite",
            )
        ]

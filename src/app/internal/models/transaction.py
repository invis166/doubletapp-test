from django.db import models

from app.internal.models.bank_account import BankAccount


class Transaction(models.Model):
    sender_account = models.ForeignKey(
        BankAccount,
        on_delete=models.CASCADE,
        related_name="outgoing_transactions",
    )
    receiver_account = models.ForeignKey(
        BankAccount,
        on_delete=models.CASCADE,
        related_name="incoming_transactions",
    )
    date = models.DateField(auto_now_add=True)
    amount = models.DecimalField(
        max_digits=19,
        decimal_places=4,
    )

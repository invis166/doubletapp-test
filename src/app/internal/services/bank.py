from decimal import Decimal

from django.db import transaction
from django.db.models import Q

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.models.transaction import Transaction
from app.internal.services.exceptions import BankAccountDoesNotExist, NotEnoughMoney, UserDoesNotExist


def get_account_by_number(account_number: str) -> BankAccount:
    """Return BankAccount object if it exists, else None"""
    return BankAccount.objects.filter(account_number=account_number).first()


def get_account_by_telegram_id(telegram_id: int) -> BankAccount:
    """Return BankAccount object if it exists, else None"""
    return BankAccount.objects.filter(owner__telegram_id=telegram_id).first()


def get_account_by_card_number(card_number: str) -> BankAccount:
    """Return BankAccount object if it exists, else None"""
    card = Card.objects.select_related("bank_account").filter(card_number=card_number).first()
    if card is None:
        return None

    return card.bank_account


def get_card_by_number(card_number: str) -> Card:
    """Return Card object if it exists, else None"""
    return Card.objects.filter(card_number=card_number).first()


def get_account_balance(account_number: str) -> int:
    """Return balance of a specified account or None, if that account doesn't exist"""
    fields = BankAccount.objects.filter(account_number=account_number).values_list("balance").first()
    if fields is None:
        return None

    return fields[0]


def get_card_balance(card_number: str) -> int:
    """Return balance of a specified card or None, if that card doesn't exist"""
    fields = Card.objects.filter(card_number=card_number).values_list("bank_account__balance").first()
    if fields is None:
        return None

    return fields[0]


def transfer_money(amount: Decimal, *, from_account: BankAccount, to_account: BankAccount):
    if to_account is None or from_account is None:
        raise UserDoesNotExist
    if from_account.balance < amount:
        raise NotEnoughMoney

    with transaction.atomic():
        transaction_obj = Transaction(sender_account=from_account, receiver_account=to_account, amount=amount)
        transaction_obj.save()
        from_account.balance -= Decimal(amount)
        to_account.balance += Decimal(amount)
        from_account.save()
        to_account.save()


def transfer_money_by_account_number(from_user_id: int, to_account_number: str, amount: float):
    from_account = get_account_by_telegram_id(from_user_id)
    to_account = get_account_by_number(to_account_number)
    transfer_money(
        amount,
        from_account=from_account,
        to_account=to_account,
    )


def transfer_money_by_telegram_id(from_user_id: int, to_user_id: int, amount: float):
    from_account = get_account_by_telegram_id(from_user_id)
    to_account = get_account_by_telegram_id(to_user_id)
    transfer_money(
        amount,
        from_account=from_account,
        to_account=to_account,
    )


def transfer_money_by_card_number(from_user_id: int, to_card_number: str, amount: float):
    from_account = get_account_by_telegram_id(from_user_id)
    to_account = get_account_by_card_number(to_card_number)
    transfer_money(
        amount,
        from_account=from_account,
        to_account=to_account,
    )


def get_account_statement(account_number: str) -> list[tuple]:
    is_account_in_transaction = Q(sender_account__account_number=account_number) | Q(
        receiver_account__account_number=account_number
    )
    transactions = (
        Transaction.objects.filter(is_account_in_transaction)
        .order_by("-date")
        .values_list("sender_account__owner__user_name", "receiver_account__owner__user_name", "date", "amount")
    )

    return list(transactions)


def get_card_account_statement(card_number: str):
    account_number = get_account_by_card_number(card_number)

    return get_account_statement(account_number)


def get_interacted_users(user_id: int) -> list[str]:
    receivers = (
        Transaction.objects.filter(sender_account__owner__telegram_id=user_id)
        .distinct()
        .values_list("receiver_account__owner__user_name")
    )
    senders = (
        Transaction.objects.filter(receiver_account__owner__telegram_id=user_id)
        .distinct()
        .values_list("sender_account__owner__user_name")
    )
    all_users = receivers.union(senders).all()

    return [field[0] for field in all_users]

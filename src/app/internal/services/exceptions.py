class NotEnoughMoney(Exception):
    pass


class UserDoesNotExist(Exception):
    pass


class CardDoesNotExist(Exception):
    pass


class BankAccountDoesNotExist(Exception):
    pass


class NotInFavourite(Exception):
    pass


class AlreadyExist(Exception):
    pass

from django.db.utils import IntegrityError

from app.internal.models.bot_user import BotUser
from app.internal.services.exceptions import AlreadyExist, NotInFavourite, UserDoesNotExist


def add_user_to_favourite(owner_id: int, favourite_id: int):
    try:
        favourite = BotUser.objects.get(telegram_id=favourite_id)
        owner = BotUser.objects.get(telegram_id=owner_id)
    except BotUser.DoesNotExist:
        raise UserDoesNotExist

    owner.favourites.add(favourite)
    owner.save()


def delete_user_from_favourite(owner_id: int, favourite_id: int):
    try:
        owner = BotUser.objects.get(telegram_id=owner_id)
        favourite = BotUser.objects.get(telegram_id=favourite_id)
    except BotUser.DoesNotExist:
        raise UserDoesNotExist

    owner.favourites.remove(favourite)
    owner.save()


def get_user_favourites_names(owner_id: int) -> list[str]:
    query = BotUser.objects.filter(telegram_id=owner_id)
    names = list(query.values_list("favourites__user_name"))
    if names == [(None,)]:
        return []

    return [field[0] for field in names]

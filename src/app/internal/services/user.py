from app.internal.models.bot_user import BotUser
from app.internal.services.exceptions import UserDoesNotExist


def get_bot_user_by_telegram_id(telegram_id: int) -> BotUser:
    return BotUser.objects.filter(telegram_id=telegram_id).first()


def get_or_create_bot_user(telegram_id: int, defaults: dict) -> tuple[BotUser, bool]:
    obj, created = BotUser.objects.get_or_create(
        telegram_id=telegram_id,
        defaults=defaults,
    )

    return obj, created


def set_user_phone_number(telegram_id: int, phone_number: str):
    try:
        user = BotUser.objects.get(telegram_id=telegram_id)
        user.phone_number = phone_number
        user.save()
    except BotUser.DoesNotExist:
        raise UserDoesNotExist

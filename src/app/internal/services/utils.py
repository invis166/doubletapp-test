import re

from telegram import Update


def validate_phone_number(number: str) -> bool:
    """Accept phone numbers in formats '89876543210' and '79876543210'"""
    if not number:
        return False

    number = re.sub(r"[^0-9]", "", number)

    return (number[0] in ("7", "8")) and len(number) == 11


def unify_phone_number(number: str) -> str:
    """Transform a valid phone number in format 89876543210"""
    number = re.sub(r"[^0-9]", "", number)

    return "8" + number[1:]


def get_command_from_update(update: Update):
    pass

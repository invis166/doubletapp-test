from functools import wraps

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.models.bot_user import BotUser


def user_exists(welcome_message_handler):
    """Redirect new users to welcome command"""

    def _inner(command_handler):
        @wraps(command_handler)
        def wrapper(update: Update, context: CallbackContext):
            user_id = update.message.from_user.id
            obj = BotUser.objects.filter(telegram_id=user_id).first()

            if obj is None:
                return welcome_message_handler(update, context)

            return command_handler(update, context)

        return wrapper

    return _inner


def phone_number_set(command_handler):
    """Does not allow users without a phone number set to use command"""

    @wraps(command_handler)
    def wrapper(update: Update, context: CallbackContext):
        user_id = update.message.from_user.id
        obj = BotUser.objects.get(telegram_id=user_id)

        if obj.phone_number == "":
            update.message.reply_text("Для начала заполни свой номер при помощи команды /set_phone")
            return

        return command_handler(update, context)

    return wrapper


def arguments_number(number: int):
    """Specify number of command arguments. If number of passed arguments doesn't match, sends message to user"""

    def _inner(command_handler):
        @wraps(command_handler)
        def wrapper(update: Update, context: CallbackContext):
            args_number = len(context.args)
            if args_number != number:
                command = update.message.text.split(" ")[0]
                update.message.reply_text(f"Неверное использование команды {command}")
                return

            command_handler(update, context)

        return wrapper

    return _inner


def phone_number_set_and_user_exists(welcome_message_handler):
    def _inner(command_handler):
        @wraps(command_handler)
        def wrapper(update: Update, context: CallbackContext):
            user_id = update.message.from_user.id
            obj = BotUser.objects.filter(telegram_id=user_id).first()

            if obj is None:
                return welcome_message_handler(update, context)

            if obj.phone_number == "":
                update.message.reply_text("Для начала заполни свой номер при помощи команды /set_phone")
                return

            return command_handler(update, context)

        return wrapper

    return _inner


# TODO: refactor repeated code

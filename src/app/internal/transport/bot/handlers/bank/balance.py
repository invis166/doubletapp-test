from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.bank import get_account_balance, get_card_balance
from app.internal.transport.bot.decorators import arguments_number, phone_number_set_and_user_exists
from app.internal.transport.bot.handlers.user import start as start_handler
from app.internal.transport.bot.messages import Replies

phone_number_set_and_user_exists = phone_number_set_and_user_exists(start_handler)


@arguments_number(1)
@phone_number_set_and_user_exists
def card_balance(update: Update, context: CallbackContext):
    card_number = context.args[0]
    balance = get_card_balance(card_number)
    if balance is None:
        update.message.reply_text(Replies.CanNotFindSpecificCard)
        return

    update.message.reply_text(Replies.get_card_balance_message(balance))


@arguments_number(1)
@phone_number_set_and_user_exists
def account_balance(update: Update, context: CallbackContext):
    account_number = context.args[0]
    balance = get_account_balance(account_number)
    if balance is None:
        update.message.reply_text(Replies.CanNotFindBankAccount)
        return

    update.message.reply_text(Replies.get_account_balance_message(balance))

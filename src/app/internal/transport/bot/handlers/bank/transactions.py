from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.bank import (
    get_account_statement,
    get_card_account_statement,
    get_interacted_users,
    transfer_money_by_account_number,
    transfer_money_by_card_number,
    transfer_money_by_telegram_id,
)
from app.internal.services.exceptions import NotEnoughMoney, UserDoesNotExist
from app.internal.transport.bot.decorators import arguments_number, phone_number_set_and_user_exists
from app.internal.transport.bot.handlers.user import start as start_handler
from app.internal.transport.bot.messages import Replies

phone_number_set_and_user_exists = phone_number_set_and_user_exists(start_handler)


class TransferMoneyBy:
    def __init__(self, transfer_handler):
        self.transfer_handler = transfer_handler

    def __call__(self, update: Update, context: CallbackContext):
        to = context.args[0]
        user_id = update.message.from_user.id
        amount = context.args[1]

        try:
            amount = float(amount)
        except ValueError:
            update.message.reply_text(Replies.InvalidArgument)
            return

        if amount <= 0:
            update.message.reply_text(Replies.NegativeAmount)
            return

        try:
            self.transfer_handler(user_id, to, amount)
        except NotEnoughMoney:
            update.message.reply_text(Replies.NotEnoughMoney)
            return
        except UserDoesNotExist:
            update.message.reply_text(Replies.CanNotFindRequisites)
            return

        update.message.reply_text(Replies.TransactionSuccess)


transfer_money_card_number = TransferMoneyBy(transfer_handler=transfer_money_by_card_number)
transfer_money_account_number = TransferMoneyBy(transfer_handler=transfer_money_by_account_number)
transfer_money_telegram_id = TransferMoneyBy(transfer_handler=transfer_money_by_telegram_id)


@phone_number_set_and_user_exists
@arguments_number(1)
def account_statement(update: Update, context: CallbackContext):
    account_number = context.args[0]
    statement = get_account_statement(account_number)

    update.message.reply_text(Replies.get_account_statement(statement))


@phone_number_set_and_user_exists
@arguments_number(1)
def card_account_statement(update: Update, context: CallbackContext):
    card_number = context.args[0]
    statement = get_card_account_statement(card_number)

    update.message.reply_text(Replies.get_account_statement(statement))


@phone_number_set_and_user_exists
@arguments_number(0)
def interacted_users(update: Update, context: CallbackContext):
    user_id = update.message.from_user.id
    interacted = get_interacted_users(user_id)

    update.message.reply_text(Replies.get_interacted_list(interacted))

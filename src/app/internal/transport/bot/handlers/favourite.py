from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.exceptions import AlreadyExist, NotInFavourite, UserDoesNotExist
from app.internal.services.favourite import add_user_to_favourite, delete_user_from_favourite, get_user_favourites_names
from app.internal.transport.bot.decorators import arguments_number, phone_number_set_and_user_exists
from app.internal.transport.bot.handlers.user import start as start_handler
from app.internal.transport.bot.messages import Replies

phone_number_set_and_user_exists = phone_number_set_and_user_exists(start_handler)


@arguments_number(1)
@phone_number_set_and_user_exists
def add_to_favourite(update: Update, context: CallbackContext):
    user_id = update.message.from_user.id
    favourite_user = int(context.args[0])
    try:
        add_user_to_favourite(user_id, favourite_user)
    except UserDoesNotExist:
        update.message.reply_text(Replies.CanNotFindUser)
        return

    update.message.reply_text(Replies.get_favourite_add_success(favourite_user))


@arguments_number(1)
@phone_number_set_and_user_exists
def delete_from_favourite(update: Update, context: CallbackContext):
    user_id = update.message.from_user.id
    favourite_user = int(context.args[0])
    delete_user_from_favourite(user_id, favourite_user)

    update.message.reply_text(Replies.get_favourite_delete_success(favourite_user))


def get_favourite_list(update: Update, context: CallbackContext):
    user_id = update.message.from_user.id
    favourites_names = get_user_favourites_names(user_id)
    if len(favourites_names) == 0:
        update.message.reply_text(Replies.FavouritesListIsEmpty)
        return

    update.message.reply_text(Replies.get_favourites_list_message(favourites_names))

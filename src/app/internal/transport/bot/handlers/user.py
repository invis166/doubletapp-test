from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user import get_bot_user_by_telegram_id, get_or_create_bot_user, set_user_phone_number
from app.internal.services.utils import unify_phone_number, validate_phone_number
from app.internal.transport.bot.decorators import (
    arguments_number,
    phone_number_set,
    phone_number_set_and_user_exists,
    user_exists,
)
from app.internal.transport.bot.messages import Replies


def start(update: Update, context: CallbackContext):
    """Greetings message"""
    user_id = update.message.from_user.id
    obj, created = get_or_create_bot_user(
        user_id,
        defaults={"first_name": update.message.from_user.first_name, "user_name": update.message.from_user.username},
    )

    if obj.phone_number == "":
        update.message.reply_text(Replies.DialogStart)
    else:
        update.message.reply_text(Replies.Welcome)


user_exists = user_exists(welcome_message_handler=start)
phone_number_set_and_user_exists = phone_number_set_and_user_exists(welcome_message_handler=start)


@user_exists
@phone_number_set
def me(update: Update, context: CallbackContext):
    """Return information about user"""
    user_id = update.message.from_user.id
    obj = get_bot_user_by_telegram_id(user_id)

    update.message.reply_text(Replies.get_user_info_message(obj))


@arguments_number(1)
@user_exists
def set_phone(update: Update, context: CallbackContext):
    """Validate and set phone number"""
    user_id = update.message.from_user.id

    phone_number = context.args[0]
    if validate_phone_number(phone_number):
        unified = unify_phone_number(phone_number)
        set_user_phone_number(user_id, unified)
        update.message.reply_text(Replies.PhoneNumberSuccess)
    else:
        update.message.reply_text(Replies.InvalidPhoneNumber)

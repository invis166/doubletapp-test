from app.internal.models.bot_user import BotUser


class Replies:
    DialogStart = (
        "Привет! Я еще не знаю твоего номера телефона, поэтому, пожалуйста, введи его "
        "при помощи команды /set_phone\n"
        "Формат номера: 89876543210"
    )
    InvalidPhoneNumber = "К сожалению, я не могу разобрать введенный номер, попробуй снова.\nФормат номера: 89876543210"
    PhoneNumberSuccess = "Номер успешно введен!"
    Welcome = "Рад снова видеть!"
    CanNotFindUser = "Не могу найти указанного пользователя"
    AlreadyInFavourite = "Указанный пользователь уже в списке избранного"
    NotInFavourite = "Указанный пользователь не находится в списке избранного"
    FavouritesListIsEmpty = "У вас нет избранных пользователей"
    CanNotFindSpecificCard = "Карта с указанным номером не найдена"
    CanNotFindBankAccount = "Счет с указанным номером не найден"
    NotEnoughMoney = "На вашем счете недостаточно средств"
    CanNotFindRequisites = "Указанные реквизиты не найдены"
    TransactionSuccess = "Транзакция успешно исполнена"
    NegativeAmount = "Сумма перевода должна быть положительной"
    InvalidArgument = "Неверное использование команды"

    @staticmethod
    def get_user_info_message(user: BotUser) -> str:
        reply = (
            "Информация о пользователе:\n"
            f"* id: {user.telegram_id}\n"
            f"* user name: {user.user_name}\n"
            f"* first name: {user.first_name}\n"
            f"* phone number: {user.phone_number}"
        )

        return reply

    @staticmethod
    def get_favourite_add_success(user_id: [int, str]) -> str:
        return f"Пользователь {user_id} успешно добавлен в избранное"

    @staticmethod
    def get_favourite_delete_success(user_id: [int, str]) -> str:
        return f"Пользователь {user_id} успешно удален из избранного"

    @staticmethod
    def get_favourites_list_message(favourites_list: list[str]) -> str:
        reply = ["Ваши избранные пользователи: ", "```"]
        for i, user in enumerate(favourites_list, 1):
            reply.append(f"{i} {user}")
        reply.append("```")

        return "\n".join(reply)

    @staticmethod
    def get_card_balance_message(balance: [int, str]) -> str:
        return f"Баланс карты: {balance:.2f} у.е."

    @staticmethod
    def get_account_balance_message(balance: [int, str]) -> str:
        return f"Баланс счета: {balance:.2f} у.е."

    @staticmethod
    def get_account_statement(statement: list[tuple]) -> str:
        res = [
            f"{date}: Transferred {amount} from {sender} to {receiver}"
            for (sender, receiver, date, amount) in statement
        ]

        return "\n".join(res)

    @staticmethod
    def get_interacted_list(interacted: list[str]) -> str:
        return "Список взаимодействовавших людей: \n" + "\n".join(interacted)

from django.http import Http404, JsonResponse
from django.views import View

from app.internal.models.bot_user import BotUser


class UserView(View):
    def get(self, *args, **kwargs):
        user_id = kwargs["user_id"]
        try:
            obj = BotUser.objects.get(telegram_id=user_id)
        except BotUser.DoesNotExist:
            raise Http404("User does not exist")

        data = {"name": obj.first_name, "phone_number": obj.phone_number, "user_id": obj.telegram_id}

        return JsonResponse(data)

from django.urls import path

from app.internal.transport.rest.handlers import UserView

urlpatterns = [
    path("users/<int:user_id>", UserView.as_view()),
]

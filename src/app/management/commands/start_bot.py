import os

import django
from django.core.management.base import BaseCommand

from app.internal.bot import TelegramBot
from config.settings import BOT_TOKEN, DOMAIN_NAME, WEBHOOK_PORT

django.setup()


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Starting telegram bot")
        bot = TelegramBot(BOT_TOKEN)
        bot.run_webhook(domain=DOMAIN_NAME, port=WEBHOOK_PORT)

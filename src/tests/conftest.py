import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card


@pytest.fixture()
@pytest.mark.django_db
def test_bot_user(user_name="test_user_name", first_name="test_first_name", telegram_id=123, phone_number=89999999999):
    user = BotUser.objects.create(
        telegram_id=telegram_id,
        user_name=user_name,
        first_name=first_name,
        phone_number=phone_number,
    )

    return user


@pytest.fixture()
@pytest.mark.django_db
def test_bot_user_second(
    user_name="test_user_name_second", first_name="test_first_name_second", telegram_id=123123, phone_number=89999999988
):
    user = BotUser.objects.create(
        telegram_id=telegram_id,
        user_name=user_name,
        first_name=first_name,
        phone_number=phone_number,
    )

    return user


@pytest.fixture()
@pytest.mark.django_db
def test_bank_account(test_bot_user: BotUser, balance=0, account_number="111111"):
    bank_account = BankAccount.objects.create(
        owner=test_bot_user,
        balance=balance,
        account_number=account_number,
    )

    return bank_account


@pytest.fixture()
@pytest.mark.django_db
def test_card(test_bank_account: BankAccount, card_number="2222222"):
    card = Card.objects.create(
        bank_account=test_bank_account,
        card_number=card_number,
    )

    return card


@pytest.fixture()
@pytest.mark.django_db
def test_bank_account_second(test_bot_user_second: BotUser, balance=0, account_number="333333"):
    bank_account = BankAccount.objects.create(
        owner=test_bot_user_second,
        balance=balance,
        account_number=account_number,
    )

    return bank_account

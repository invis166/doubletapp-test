from unittest import mock

import pytest

from app.internal.models.bot_user import BotUser
from tests.integration.utils import get_message_mock, get_update_mock


@pytest.fixture()
def update_mock() -> mock.Mock:
    return get_update_mock()


@pytest.fixture()
def context_mock() -> mock.Mock:
    return mock.Mock()


@pytest.fixture()
def existing_user_update_mock(test_bot_user: BotUser) -> mock.Mock:
    return get_update_mock(test_bot_user.telegram_id, test_bot_user.first_name, test_bot_user.user_name)

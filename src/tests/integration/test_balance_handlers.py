from unittest import mock

import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.transport.bot.handlers.bank.balance import account_balance, card_balance
from app.internal.transport.bot.messages import Replies


@pytest.mark.django_db
@pytest.mark.integration
def test_card_balance_reply_when_card_exist(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_card: Card
):
    context_mock.args = [test_card.card_number]
    card_balance(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(
        Replies.get_card_balance_message(test_card.bank_account.balance)
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_card_balance_reply_when_card_does_not_exist(existing_user_update_mock: mock.Mock, context_mock: mock.Mock):
    context_mock.args = [12345]
    card_balance(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.CanNotFindSpecificCard)


@pytest.mark.django_db
@pytest.mark.integration
def test_account_balance_reply_when_card_does_not_exist(existing_user_update_mock: mock.Mock, context_mock: mock.Mock):
    context_mock.args = [12345]
    account_balance(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.CanNotFindBankAccount)


@pytest.mark.django_db
@pytest.mark.integration
def test_account_balance_reply_when_account_exist(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bank_account: BankAccount
):
    context_mock.args = [test_bank_account.account_number]
    account_balance(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(
        Replies.get_account_balance_message(test_bank_account.balance)
    )

from unittest import mock

import pytest

from app.internal.models.bot_user import BotUser
from app.internal.transport.bot.handlers.favourite import add_to_favourite, delete_from_favourite, get_favourite_list
from app.internal.transport.bot.messages import Replies


@pytest.mark.django_db
@pytest.mark.integration
def test_add_to_favourite_reply(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser, test_bot_user_second: BotUser
):
    context_mock.args = [test_bot_user_second.telegram_id]
    add_to_favourite(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(
        Replies.get_favourite_add_success(test_bot_user_second.telegram_id)
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_add_to_favourite_update_favourites_list(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser, test_bot_user_second: BotUser
):
    context_mock.args = [test_bot_user_second.telegram_id]
    add_to_favourite(existing_user_update_mock, context_mock)

    test_bot_user.refresh_from_db()
    favourites = list(test_bot_user.favourites.all())

    assert len(favourites) == 1
    assert favourites[0] == test_bot_user_second


@pytest.mark.django_db
@pytest.mark.integration
def test_add_to_favourite_reply_when_invalid_favourite_id(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser
):
    not_existing_id = "99991111"
    context_mock.args = [not_existing_id]
    add_to_favourite(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.CanNotFindUser)


@pytest.mark.django_db
@pytest.mark.integration
def test_delete_from_favourite_reply(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser, test_bot_user_second: BotUser
):
    test_bot_user.favourites.add(test_bot_user_second)
    test_bot_user.save()
    context_mock.args = [test_bot_user_second.telegram_id]
    delete_from_favourite(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(
        Replies.get_favourite_delete_success(test_bot_user_second.telegram_id)
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_get_favourites_list_when_empty(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser
):
    get_favourite_list(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.FavouritesListIsEmpty)

from unittest import mock

import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.transport.bot.handlers.bank.transactions import (
    transfer_money_account_number,
    transfer_money_card_number,
    transfer_money_telegram_id,
)
from app.internal.transport.bot.messages import Replies


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_account_number_reply_on_success(
    test_bank_account: BankAccount,
    test_bank_account_second: BankAccount,
    existing_user_update_mock: mock.Mock,
    context_mock: mock.Mock,
):
    test_bank_account_second.balance = 100
    test_bank_account_second.save()
    test_bank_account.balance = 100
    test_bank_account.save()
    context_mock.args = [test_bank_account_second.account_number, 100]
    transfer_money_account_number(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.TransactionSuccess)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_account_number_reply_on_negative_amount(
    test_bank_account: BankAccount,
    test_bank_account_second: BankAccount,
    existing_user_update_mock: mock.Mock,
    context_mock: mock.Mock,
):
    context_mock.args = [test_bank_account_second.account_number, -10]
    transfer_money_account_number(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.NegativeAmount)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_account_number_reply_on_not_enough_money(
    test_bank_account: BankAccount,
    test_bank_account_second: BankAccount,
    existing_user_update_mock: mock.Mock,
    context_mock: mock.Mock,
):
    context_mock.args = [test_bank_account_second.account_number, 10]
    transfer_money_account_number(existing_user_update_mock, context_mock)

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.NotEnoughMoney)

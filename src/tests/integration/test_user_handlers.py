from unittest import mock

import pytest

from app.internal.models.bot_user import BotUser
from app.internal.transport.bot.handlers.user import me, set_phone, start
from app.internal.transport.bot.messages import Replies
from tests.integration.utils import get_update_mock


@pytest.mark.integration
@pytest.mark.django_db
def test_start_reply_when_new_user(update_mock: mock.Mock, context_mock: mock.Mock):
    start(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(Replies.DialogStart)


@pytest.mark.integration
@pytest.mark.django_db
def test_start_new_user_created(update_mock: mock.Mock, context_mock: mock.Mock):
    start(update_mock, context_mock)
    assert BotUser.objects.get(telegram_id=update_mock.message.from_user.id)


@pytest.mark.integration
@pytest.mark.django_db
def test_start_reply_when_existing_user(
    existing_user_update_mock: mock.Mock,
    context_mock: mock.Mock,
):
    start(existing_user_update_mock, context_mock)
    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.Welcome)


@pytest.mark.integration
@pytest.mark.django_db
def test_me_redirect_to_start_when_new_user(update_mock: mock.Mock, context_mock: mock.Mock):
    me(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(Replies.DialogStart)


@pytest.mark.integration
@pytest.mark.django_db
def test_me_reply_when_existing_user(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser
):
    me(existing_user_update_mock, context_mock)
    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.get_user_info_message(test_bot_user))


@pytest.mark.integration
@pytest.mark.django_db
def test_set_phone_changes_number(
    existing_user_update_mock: mock.Mock, context_mock: mock.Mock, test_bot_user: BotUser
):
    new_number = "81234445566"
    context_mock.args = [new_number]
    set_phone(existing_user_update_mock, context_mock)
    test_bot_user.refresh_from_db()

    existing_user_update_mock.message.reply_text.assert_called_once_with(Replies.PhoneNumberSuccess)
    assert test_bot_user.phone_number == new_number

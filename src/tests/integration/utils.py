from unittest import mock


def get_message_mock(id=12345, first_name="test_mock_name", username="test_mocK_username") -> mock.Mock:
    from_user_mock = mock.Mock(
        id=id,
        first_name=first_name,
        username=username,
    )
    message_mock = mock.Mock()
    message_mock.from_user = from_user_mock

    return message_mock


def get_update_mock(id=12345, first_name="test_mock_name", username="test_mocK_username") -> mock.Mock:
    message_mock = get_message_mock(id, first_name, username)
    update_mock = mock.Mock()
    update_mock.message = message_mock

    return update_mock

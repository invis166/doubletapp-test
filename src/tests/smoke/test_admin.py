import pytest
from django.test import Client


@pytest.mark.smoke
def test_site_smoke():
    response = Client().get("/admin/")
    assert response.status_code == 302

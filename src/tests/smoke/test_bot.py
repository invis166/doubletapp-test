from app.internal.bot import TelegramBot
from config.settings import BOT_TOKEN


def test_bot():
    bot = TelegramBot(BOT_TOKEN)
    assert bot._updater.dispatcher.bot.get_webhook_info()

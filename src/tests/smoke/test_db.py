import pytest

from app.internal.models.bot_user import BotUser


@pytest.mark.smoke
@pytest.mark.django_db
def test_database_smoke():
    BotUser.objects.create(telegram_id=1234, user_name="test", first_name="test")

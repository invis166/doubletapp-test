import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.bot_user import BotUser
from app.internal.models.card import Card
from app.internal.services.bank import (
    get_account_balance,
    get_account_by_card_number,
    get_account_by_number,
    get_account_by_telegram_id,
    get_card_balance,
    get_card_by_number,
    transfer_money,
)
from app.internal.services.exceptions import NotEnoughMoney, UserDoesNotExist
from tests.unit.utils import set_balance_and_save, set_balance_to_each_account_and_save


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_by_number_return_none_when_does_not_exist():
    obj = get_account_by_number("9999999")

    assert obj is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_by_number_return_requested_object(test_bank_account: BankAccount):
    obj = get_account_by_number(test_bank_account.account_number)

    assert obj == test_bank_account


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_by_card_return_none_when_does_not_exist():
    obj = get_account_by_card_number("9999999")

    assert obj is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_by_card_return_related_account(test_card: Card):
    obj = get_account_by_card_number(test_card.card_number)

    assert obj == test_card.bank_account


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_by_tg_return_none_when__does_not_exist():
    obj = get_account_by_telegram_id(9999999)

    assert obj is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_by_tg_return_related_account(test_bot_user: BotUser, test_bank_account: BankAccount):
    obj = get_account_by_telegram_id(test_bot_user.telegram_id)

    assert obj.owner == test_bot_user


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_by_number_return_none_when_does_not_exist():
    obj = get_card_by_number("9999")

    assert obj is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_by_number_return_requested_object(test_card: Card):
    obj = get_card_by_number(test_card.card_number)

    assert obj == test_card


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_balance_return_none_when_does_not_exist():
    obj = get_account_balance("99999")

    assert obj is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_balance_return_related_balance(test_bank_account: BankAccount):
    obj = get_account_balance(test_bank_account.account_number)

    assert obj == test_bank_account.balance


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_balance_return_none_when_does_not_exist():
    obj = get_card_balance("99999")

    assert obj is None


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_balance_return_related_balance(test_card: Card):
    obj = get_card_balance(test_card.card_number)

    assert obj == test_card.bank_account.balance


@pytest.mark.unit
@pytest.mark.django_db
def test_transfer_money_correctly(test_bank_account: BankAccount, test_bank_account_second: BankAccount):
    first, second = set_balance_to_each_account_and_save(100, test_bank_account, test_bank_account_second)

    transfer_money(100, from_account=first, to_account=second)
    first.refresh_from_db()
    second.refresh_from_db()

    assert first.balance == 0
    assert second.balance == 200


@pytest.mark.unit
@pytest.mark.django_db
def test_transfer_money_raise_when_not_enough_money(
    test_bank_account: BankAccount, test_bank_account_second: BankAccount
):
    first = set_balance_and_save(0, test_bank_account)
    with pytest.raises(NotEnoughMoney):
        transfer_money(100, from_account=first, to_account=test_bank_account_second)


@pytest.mark.unit
@pytest.mark.django_db
def test_transfer_money_raise_when_sender_does_not_exist(test_bank_account: BankAccount):
    with pytest.raises(UserDoesNotExist):
        transfer_money(100, from_account=None, to_account=test_bank_account)


@pytest.mark.unit
@pytest.mark.django_db
def test_transfer_money_raise_when_receiver_does_not_exist(test_bank_account: BankAccount):
    with pytest.raises(UserDoesNotExist):
        transfer_money(100, from_account=test_bank_account, to_account=None)

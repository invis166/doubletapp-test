import pytest

from app.internal.models.bot_user import BotUser
from app.internal.services.exceptions import AlreadyExist, UserDoesNotExist
from app.internal.services.favourite import add_user_to_favourite, delete_user_from_favourite, get_user_favourites_names


@pytest.mark.django_db
@pytest.mark.unit
def test_add_to_favourite_raise_when_favourite_user_does_not_exist(test_bot_user: BotUser):
    not_existing_id = 123456789
    assert test_bot_user.telegram_id != not_existing_id

    with pytest.raises(UserDoesNotExist):
        add_user_to_favourite(test_bot_user.telegram_id, not_existing_id)


@pytest.mark.django_db
@pytest.mark.unit
def test_add_to_favourite_extend_favourites_list_by_one(test_bot_user: BotUser, test_bot_user_second: BotUser):
    before = set(test_bot_user.favourites.all())
    add_user_to_favourite(test_bot_user.telegram_id, test_bot_user_second.telegram_id)
    after = set(test_bot_user.favourites.all())
    difference = after - before

    assert len(difference) == 1
    assert list(difference)[0] == test_bot_user_second


@pytest.mark.django_db
@pytest.mark.unit
def test_delete_user_from_favourite_raise_when_favourite_does_not_exist(test_bot_user: BotUser):
    with pytest.raises(UserDoesNotExist):
        delete_user_from_favourite(test_bot_user.telegram_id, 55555)


@pytest.mark.django_db
@pytest.mark.unit
def test_delete_user_from_favourite_raise_when_owner_does_not_exist(test_bot_user: BotUser):
    with pytest.raises(UserDoesNotExist):
        add_user_to_favourite(222222, test_bot_user.telegram_id)


@pytest.mark.django_db
@pytest.mark.unit
def test_delete_user_from_favourite_decrease_favourites_list_by_one(
    test_bot_user: BotUser, test_bot_user_second: BotUser
):
    add_user_to_favourite(test_bot_user.telegram_id, test_bot_user_second.telegram_id)
    before = set(test_bot_user.favourites.all())
    delete_user_from_favourite(test_bot_user.telegram_id, test_bot_user_second.telegram_id)
    after = set(test_bot_user.favourites.all())
    difference = before - after

    assert len(difference) == 1
    assert list(difference)[0] == test_bot_user_second


@pytest.mark.django_db
@pytest.mark.unit
def test_get_favourites_list_return_empty_list_when_no_favourites(test_bot_user: BotUser):
    favourites = get_user_favourites_names(test_bot_user.telegram_id)

    assert favourites == []


@pytest.mark.django_db
@pytest.mark.unit
def test_get_favourites_list_extend_when_add_favourite_user(test_bot_user: BotUser, test_bot_user_second: BotUser):
    before = set(get_user_favourites_names(test_bot_user.telegram_id))
    add_user_to_favourite(test_bot_user.telegram_id, test_bot_user_second.telegram_id)
    after = set(get_user_favourites_names(test_bot_user.telegram_id))
    difference = after - before

    assert len(difference) == 1
    assert list(difference)[0] == test_bot_user_second.user_name


@pytest.mark.django_db
@pytest.mark.unit
def test_get_favourites_list_do_not_extend_after_adding_existing_favourite(
    test_bot_user: BotUser, test_bot_user_second: BotUser
):
    add_user_to_favourite(test_bot_user.telegram_id, test_bot_user_second.telegram_id)
    before = get_user_favourites_names(test_bot_user.telegram_id)
    add_user_to_favourite(test_bot_user.telegram_id, test_bot_user_second.telegram_id)
    after = get_user_favourites_names(test_bot_user.telegram_id)

    assert before == after

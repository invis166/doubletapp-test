import pytest

from app.internal.models.bot_user import BotUser
from app.internal.services.exceptions import UserDoesNotExist
from app.internal.services.user import get_bot_user_by_telegram_id, get_or_create_bot_user, set_user_phone_number


@pytest.mark.django_db
@pytest.mark.unit
def test_get_by_tg_id_return_none_when_user_does_not_exist():
    obj = get_bot_user_by_telegram_id(telegram_id=9999)

    assert obj is None


@pytest.mark.django_db
@pytest.mark.unit
def test_get_by_tg_id_return_expected_user(test_bot_user: BotUser):
    user = get_bot_user_by_telegram_id(telegram_id=test_bot_user.telegram_id)

    assert user == test_bot_user


@pytest.mark.django_db
@pytest.mark.unit
def test_get_or_create_return_same_user_and_false_when_user_exist(test_bot_user: BotUser):
    user, created = get_or_create_bot_user(
        telegram_id=test_bot_user.telegram_id,
        defaults={
            "user_name": test_bot_user.user_name,
            "first_name": test_bot_user.first_name,
            "phone_number": test_bot_user.phone_number,
        },
    )

    assert not created
    assert user == test_bot_user


@pytest.mark.django_db
@pytest.mark.unit
def test_get_or_create_return_new_user_and_true_whe_user_does_not_exist():
    telegram_id = 99999
    user, created = get_or_create_bot_user(
        telegram_id=telegram_id,
        defaults={
            "user_name": "some_user_name",
            "first_name": "some_first_name",
            "phone_number": "81111111111",
        },
    )

    assert created
    assert user.telegram_id == telegram_id


@pytest.mark.django_db
@pytest.mark.unit
def test_set_phone_number_raise_when_user_does_not_exist():
    with pytest.raises(UserDoesNotExist):
        set_user_phone_number(telegram_id=999999, phone_number="81234567890")


@pytest.mark.django_db
@pytest.mark.unit
def test_set_phone_number_change_phone_number_on_existing_user(test_bot_user: BotUser):
    before = test_bot_user.phone_number
    new_number = "81234567890"

    assert before != new_number  # todo: fix with params

    set_user_phone_number(test_bot_user.telegram_id, new_number)
    test_bot_user = BotUser.objects.get(telegram_id=test_bot_user.telegram_id)

    assert test_bot_user.phone_number == new_number

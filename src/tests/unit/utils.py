from app.internal.models.bank_account import BankAccount


def set_balance_to_each_account_and_save(
    balance: float, first: BankAccount, second: BankAccount
) -> tuple[BankAccount, BankAccount]:
    first = set_balance_and_save(balance, first)
    second = set_balance_and_save(balance, second)

    return first, second


def set_balance_and_save(balance: float, account: BankAccount) -> BankAccount:
    account.balance = balance
    account.save()

    return account
